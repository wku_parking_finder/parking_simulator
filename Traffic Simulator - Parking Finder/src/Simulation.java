import java.util.concurrent.TimeUnit;
import java.sql.*;

public class Simulation {
	
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost/wku_parking_finder";
	
	static final String USER = "pf_admin";
	static final String PASS = "67StPyU7bGXMVGT1";
	
	static Connection conn = null;
	static Statement stmt = null;

	public static void main(String[] a){
		
		Vehicle[] cars = new Vehicle[2000];
		ParkingLot[] lots = new ParkingLot[100];
	
		lots = initLots(lots);
		lots = resetLots(lots);
		updateDatabase(lots);
			
		double lambda = 0.65;
		
		/*Initialize vehicles*/
		for(int i = 0; i < cars.length; i++){
			//Give car random permit ID between 1-13
			cars[i] = new Vehicle(i+1, (int )(Math.random() * 14));
		}
		
		
		//Print Vehicle Statistics
		System.out.println("Printing randomized vehicle stats...");
		printVehicleStats(cars);
		
		//Begin Simulation
		while(true){
			for(Vehicle car : cars){
				for(ParkingLot lot : lots){
					if(lot != null && !car.isParked() && (Math.random() > lambda))
						car.enterLot(lot);
					else if(lot != null && car.isParked() && (Math.random() < lambda))
						car.exitLot(lot);
				}
			}
			
			System.out.println("Simulation completed one pass. Printing current lot information...\n");
			for(ParkingLot lot : lots){
				if(lot != null)
					System.out.println(lot.toString());
			}
			//Update DB after each pass of simulation
			System.out.println("Updating database information...");
			updateDatabase(lots);

			//Wait specified time between each pass, can be lowered for a quicker effect.
			try {
				TimeUnit.SECONDS.sleep(6);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	//Method to string-ify randomly generated vehicle stats
	//Printed on start of simulation to give user better idea of
	//distribution of cars
	public static void printVehicleStats(Vehicle[] c){
		int totASG = 0;
		int totGaH = 0;
		int totGR = 0;
		int totFS1 = 0;
		int totFS2 = 0;
		int totFS3 = 0;
		int totGH = 0;
		int totH = 0;
		int totC1 = 0;
		int totC2 = 0;
		int totAP = 0;
		int totPublic = 0;
		int totW = 0;
		
		for(Vehicle car : c){
			switch(car.getPermit()){
			case 1: totASG++; break;
			case 2: totGaH++; break;
			case 3: totGR++; break;
			case 4: totFS1++; break;
			case 5: totFS2++; break;
			case 6: totFS3++; break;
			case 7: totGH++; break;
			case 8: totH++; break;
			case 9: totC1++; break;
			case 10: totC2++; break;
			case 11: totAP++; break;
			case 12: totPublic++; break;
			case 13: totW++; break;
			default: break;
			}
		}
		
		System.out.println("Total ASG: " + totASG);
		System.out.println("Total GaH: " + totGaH);
		System.out.println("Total GR: " + totGR);
		System.out.println("Total FS1: " + totFS1);
		System.out.println("Total FS2: " + totFS2);
		System.out.println("Total FS3: " + totFS3);
		System.out.println("Total GH: " + totGH);
		System.out.println("Total H: " + totH);
		System.out.println("Total C1: " + totC1);
		System.out.println("Total C2: " + totC2);
		System.out.println("Total AP: " + totAP);
		System.out.println("Total Public: " + totPublic);
		System.out.println("Total W: " + totW);
	}
	
	//Method that accepts ParkingLot[] and updates each floor to DB
	public static void updateDatabase(ParkingLot[] L){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			String sql;
			ParkingLotFloor tempFloor = null;
			
			
			//Loop through ParkingLotFloors
			for(ParkingLot lot : L){
				if(lot != null){
					for(int i = 0; i < lot.getNumFloors(); i++){
						tempFloor = lot.getFloor(i);
						sql = "UPDATE parking_lot_floors "
								+"SET available_spots = " + tempFloor.getAvailSpots()
								+" WHERE floor_id = " + tempFloor.getFloorID();
						stmt.executeUpdate(sql);
					}
				}
			}
		}catch(SQLException se){
			se.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}
		}
		
	}
	
	public static ParkingLot[] resetLots(ParkingLot[] l) {
		
		ParkingLotFloor tempFloor = null;
		
		
		//Loop through ParkingLotFloors
		for(ParkingLot lot : l){
			if(lot != null){
				for(int i = 0; i < lot.getNumFloors(); i++){
					tempFloor = lot.getFloor(i);
					tempFloor.setAvailSpots(tempFloor.getTotalSpots());
				}
			}
		}
		 return l;
	}
	
	public static ParkingLot[] initLots(ParkingLot[] l) {
		
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			
			System.out.println("Creating Statement...");
			stmt = conn.createStatement();
			
			String sql;
			sql = "SELECT lot_id, lot_name FROM parking_lots WHERE closed = 0";
			ResultSet rs = stmt.executeQuery(sql);
			
			//Create ParkingLot Array
			int count = 0;
			while(rs.next()){
				int id = rs.getInt("lot_id");
				String name = rs.getString("lot_name");
				
				l[count] = new ParkingLot(name, id);
				count++;
			}
			
			sql = "SELECT lot_id, floor_id, permit_id, closed, total_spots, available_spots "
					+ "FROM parking_lot_floors";
			
			rs = stmt.executeQuery(sql);
			
			//Add floors to ParkingLots
			while(rs.next()){
				int lid = rs.getInt("lot_id");
				int flid = rs.getInt("floor_id");
				int pid = rs.getInt("permit_id");
				int closed = rs.getInt("closed");
				int tspot = rs.getInt("total_spots");
				int aspot = rs.getInt("available_spots");
				
				boolean open;
				
				if(closed > 0)
					open = true;
				else
					open = false;
				//Insert Floor info into correct lot
				for(ParkingLot lot : l){
					if(lot != null && lot.getID() == lid)
						lot.addFloor(flid, pid, tspot, aspot, open);
				}
			}
			
			rs.close();
			stmt.close();
			conn.close();
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}//Do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try
		return l;
	}
}
