public class ParkingLot {
	
	private String lotName;
	private int lotID;
	private int numFloors = 0;
	
	//ParkingLot contains private array of ParkingLotFloors each floor is specific to the Lot
	private ParkingLotFloor[] floors = new ParkingLotFloor[10];
	
	public ParkingLot(String n, int i){
		lotName = n;
		lotID = i;
	}
	
	//Getts for Lot info
	public String getName() {
		return lotName;
	}
	
	public int getID() {
		return lotID;
	}
	
	//Adds ParkingLotFloor to array
	public void addFloor(int id, int p, int ts, int as, boolean o) {
		if(numFloors < 10){
			floors[numFloors] = new ParkingLotFloor(id, p, ts, as, o);
			numFloors++;
		}
		else
			System.out.println("Lot - " + lotName + ": Cannot add more floors!");
	}
	
	//returns copy of ParkingLotFloor based on ID
	public ParkingLotFloor getFloor(int id) {
		return floors[id];
	}
	
	//accepts copy of ParkingLotFloor and updates floor[id] to match that copy
	public void updateFloor(int id, ParkingLotFloor p) {
		floors[id] = p;
	}
	
	public int getNumFloors(){
		return numFloors;
	}
	
	
	//toString method for ParkingLot Class
	//Prints info on each floor contained in Lot
	public String toString(){
		String tempString = "-----" + lotName + ":" + lotID + "-----\n";
		for (ParkingLotFloor floor : floors){
			if(floor != null){
			tempString += "\tFloor ID: " + floor.getFloorID() +
							" || Permit Type: " + floor.getPermit() +
							" || Total Spots: " + floor.getTotalSpots() +
							" || Available Spots: " + floor.getAvailSpots() +
							" || Closed: " + floor.getStatus() + "\n";
			}
		}
		return tempString;		
	}

}
