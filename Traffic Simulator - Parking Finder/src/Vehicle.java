
public class Vehicle {
	
	private int vehicleID;
	private int permitType;
	private String location;
	private int currentFloorID;
	private boolean isParked;
	
	public Vehicle(int i, int p){
		vehicleID = i;
		permitType = p;
	}
	
	public int getVehicleID(){
		return vehicleID;
	}
	
	public int getPermit(){
		return permitType;
	}
	
	public boolean isParked(){
		return isParked;
	}
	
	//If car is not parked, has correct permit type and there are spots available
	//Decrement available spots, update location/status
	public boolean enterLot(ParkingLot p){
		for(int i = 0; i < p.getNumFloors(); i++){
			ParkingLotFloor tempFloor = p.getFloor(i);
			if(tempFloor.checkPermit(permitType) && tempFloor.getAvailSpots() > 0 && !isParked){
				tempFloor.addCar();
				location = p.getName();
				currentFloorID = i;
				isParked = true;
				p.updateFloor(i, tempFloor);
				System.out.println("Parking Car " + vehicleID + " at " + p.getName() + " Floor: " + i + "(ID: " + tempFloor.getFloorID() + ")");
			}	
		}
		return isParked;
	}
	
	//If car is parked at specified lot
	//Increment available spots, update location/status
	public boolean exitLot(ParkingLot p){
		if(isParked && location == p.getName()){
			ParkingLotFloor tempFloor = p.getFloor(currentFloorID);
			tempFloor.removeCar();
			location = "Not Parked";
			p.updateFloor(currentFloorID, tempFloor);
			currentFloorID = -1;
			isParked = false;
			System.out.println("Car " + vehicleID + " leaving lot " + p.getName());
		}
		return !isParked;
	}

}
