public class ParkingLotFloor {
	
	private int floorID;
	private int permitType;
	private int numTotalSpots;
	private int numAvailSpots;
	private boolean isOpen;
	
	public ParkingLotFloor(int i, int p, int t, int a, boolean o){
		
		floorID = i;
		permitType = p;
		numTotalSpots = t;
		numAvailSpots = a;
		isOpen = o;
		
	}
	
	//Various getters for floor info.
	////////////////////////////////
	public int getFloorID(){
		return floorID;
	}
	public int getTotalSpots(){
		return numTotalSpots;
	}
	
	public int getAvailSpots(){
		return numAvailSpots;
	}
	
	public void setAvailSpots(int i) {
		this.numAvailSpots = i;
	}
	
	//Returns true if vehicle permit type is compatible with floor permit type
	public boolean checkPermit(int p){
		if(p == permitType)
			return true;
		else
			return false;
	}
	
	public boolean getStatus(){
		return isOpen;
	}
	
	//Methods to increment/decrement numAvailSpots
	//Called when cars enter or exit floor
	public void addCar(){
		numAvailSpots--;
	}
	
	public void removeCar(){
		numAvailSpots++;
	}
	
	public int getPermit(){
		return permitType;
	}
	
	public String getPermitName(){
		return convertPermitToString(permitType);
	}
	//Accepts int and returns string corresponding to the Lot/Key pairs in DB
	private String convertPermitToString(int p){
		
		switch (p) {
		case 1: return "ASG";
		case 2: return "Gated Hilltop";
		case 3: return "Gated Regents";
		case 4: return "FS1";
		case 5: return "FS2";
		case 6: return "FS3";
		case 7: return "GH";
		case 8: return "Housing";
		case 9: return "C1";
		case 10: return "C2";
		case 11: return "AP";
		case 12: return "public";
		case 13: return "W";
		default: return "Invalid Permit Type";
		}
		
	}
}
